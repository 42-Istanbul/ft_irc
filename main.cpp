#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>

#include <fcntl.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h> // inet_pton

#define BUFF_LEN 256

int	main(int ac, char **av)
{
	if (ac != 3)
	{
		std::cerr << "Usage ./ircserv <port> <password>" << std::endl;
		return (-1);
	}

	struct addrinfo	hints, *res;
	int	status;
	int	sfd;
	struct sockaddr_storage	incoming_addr;
	socklen_t	addr_size;
	int	accepted_fd;

	// Prepare addrinfo for socket
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use localhost

	if ((status = getaddrinfo(NULL, av[1], &hints, &res)) == -1)
	{
		std::cerr << gai_strerror(status) << std::endl;
		exit(-1);
	}

	// Create a socket
	sfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

	// Bind socket to IP:port
	bind(sfd, res->ai_addr, res->ai_addrlen);

	// lose binding error
	int yes = 1;
	if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)))
	{
		perror("setsockopt");
		exit(1);
	}

	// listen
	if (listen(sfd, 10) == -1)
	{
		perror(NULL);
		exit(1);
	}

	// accept
	addr_size = sizeof incoming_addr;
	accepted_fd = accept(sfd, (struct sockaddr *)&incoming_addr, &addr_size);
	fcntl(accepted_fd, F_SETFL, O_NONBLOCK);

	// communication
	char buf[BUFF_LEN];
	int	bytes_recv;

	while (strcmp(buf, "exit") != 0)
	{
		memset(buf, 0, BUFF_LEN);
		bytes_recv = recv(sfd, buf, BUFF_LEN, 0);
		if (bytes_recv == -1)
		{
			std::cerr << "Error in message" << std::endl;
			exit(1);
		}
		if (bytes_recv > 0)
			std::cout << "Received message: " << buf << "message len: " << bytes_recv << std::endl;
		send(accepted_fd, buf, bytes_recv + 1, 0);
	}
	freeaddrinfo(res);
	close(accepted_fd);
	close(sfd);
	return (0);
}
